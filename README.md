# Scheduled Messages Sending
 This Python Program sends a day-end report message every 7pm (sharp!) to a recipient. The program will prevent the sending of late or early report messages.
 