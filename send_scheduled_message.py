from skpy import Skype

import data
import schedule, time
import sys, os


def send_message(msg):
    print("done")
    print("*** Sending Message...")
    contact.chat.sendMsg(msg)
    print("done\n*** Message Sent ***\n{}".format(msg))
    sys.exit()


if __name__ == '__main__':
    if os.getenv('VIRTUAL_ENV'):
        print('Using Virtualenv')
    else:
        print('Not using Virtualenv')

    print("*** Logging in...", end='')
    email = data.email
    password = data.password
    sk = Skype(email, password)
    print("done")

    print("*** Reading message file...", end='')
    with open('message.txt', 'r') as f:
        message = f.read()
    print("done")

    print("*** Fetching Recipient...", end='')
    send_to = data.recipient
    contact = sk.contacts[send_to]
    print("done")

    print("*** Creating Schedule...", end='')
    schedule.every().day.at("19:00").do(lambda: send_message(message))
    print("done")

    print("*** Awaiting Schedule...")
    while True:
        schedule.run_pending()
        time.sleep(1)
